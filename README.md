# Qt+FFmpeg实现RTMP流媒体播放器

## 项目简介

本项目是一个基于Qt框架和FFmpeg音视频解码库实现的RTMP流媒体播放器。通过该播放器，用户可以实时拉取RTMP视频流并在界面上进行播放和显示。

## 功能特点

- **实时拉流**：支持从RTMP服务器实时拉取视频流。
- **视频解码**：利用FFmpeg库对拉取的视频流进行解码。
- **界面显示**：通过Qt框架将解码后的视频流显示在用户界面上。

## 使用说明

1. **克隆仓库**：
   ```bash
   git clone https://github.com/yourusername/your-repo.git
   ```

2. **编译与运行**：
   - 确保已安装Qt和FFmpeg库。
   - 进入项目目录，使用Qt Creator打开项目文件并编译运行。

3. **配置RTMP地址**：
   - 在代码中配置RTMP流地址，例如：
     ```cpp
     const char* rtmpUrl = "rtmp://your-rtmp-server/live/stream";
     ```

4. **启动播放器**：
   - 编译成功后，运行程序即可开始播放RTMP流媒体。

## 依赖库

- **Qt**：用于构建用户界面和处理图形显示。
- **FFmpeg**：用于音视频流的解码和处理。

## 贡献

欢迎大家贡献代码、提出问题或建议。请通过GitHub的Issue和Pull Request功能进行交流。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

---

希望这个项目能帮助你更好地理解和实现RTMP流媒体播放器。如果有任何问题或建议，请随时联系我们！